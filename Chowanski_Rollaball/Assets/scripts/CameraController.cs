﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour
{

    public GameObject player;

    private Vector3 offset;

    void Start()
    {
        //making sure the camera does not roll
        offset = transform.position - player.transform.position;
    }

    void LateUpdate()
    {
        // always follows the player
        transform.position = player.transform.position + offset;
    }
}